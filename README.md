#Wordle
## Descripcion del proyecto
El proyecto es una version de consola del conocido juego Wordle. Cuya finalidad es adivinar la palabra oculta antes de quedarse sin intentos.

## Instalacion
- Descargar el proyecto y tener el editor de codigo intellij IDEA Instalado.
- Ir a Archivo, abrir y selecionar la carpeta del proyecto.
- Ejecutar el programa

## Funcionamiento y reglas del juego
Empieza la partida y el usuario tiene 6 intentos para adivinar la palabra secreta.
Cuando introduce una palabra se comprueba si es la palabra oculta, si no lo es
se imprime la palabra del usuario pintando en amarillo las letras de la palabra del usuario
que si contiene la palabra oculta pero no esta en la misma posicion.
Y pintando de verde las letras que si estan en la misma posicion.

## Partes y proceso de creacion
La primera parte es definir las variables que vamos a manejar. Para ello he tenido en cuenta:
- variables que deberian estar inicializadas por defecto
- variables introducidas por el usuario
- avaluacion de errores y manejo de estos

La segunda parte ha sido determinar la interaccion con el usuario.

El usuario ha de introducir una palabra. La cual se ha de comprobar sicumple unos requisitos antes de entrar a comprobar si se ha acertado.

Logica 

Para comprobar que la palabra introducida por el usuario es valida se comprueba que:
- este en minusculas(para hacer el trabajo mas facil)
- su medida sea 5
- contenga solo letras

Una vez se cumplen estos requisitos se comprueba la palabra del usuario letra por letra si estas estan en la palabra oculta.
