import java.util.Scanner
import kotlin.random.Random

/*
Author: Diego Arteaga
Tittle: Wordle Game
 */
fun main() {

    val scanner = Scanner(System.`in`)

    //val userWord = scanner.nextLine()
    var userWordTest = "vorva"
    val theWord = "volvo"
    val words = arrayOf(
        "angel",
        "bueno",
        "etapa",
        "ocaso",
        "adios",
        "ebano",
        "ideas",
        "racha",
        "radio",
        "radar",
        "sabio",
        "babel",
        "cabal",
        "cable",
        "damas",
        "elite",
        "diosa",
        "baile",
        "jadeo",
        "noble"
    )
    val yellow = "\u001b[43m"
    val green = "\u001b[42m"
    val reset = "\u001b[0m"
    val gray = "\u001b[47m"
    var win = false
    var game = true
    var lives = 6
    val randomWord = List(1) { Random.nextInt(0, 21) }
    val hiddenWord = words[randomWord[0]]
    var abc = "qwertyuiopasdfghjklzxcvbnm"
    var itsIn = 0
    println("Hola! Bienvenido a Wordle. Por favor adivina la palabra oculta introduciendo una de 5 letras")
    do {
        itsIn = 0
        var userWord = scanner.nextLine()
        userWord = userWord.lowercase()
        for (i in userWord.indices){
            if (abc.contains(userWord[i])){
                itsIn ++
            }
        }
        if (userWord.length == 5 && itsIn == 5){
            for (i in hiddenWord.indices){
                if (userWord==hiddenWord){
                    print(green + "${hiddenWord[i]}" + reset)
                    win = true
                    game = false
                }else if(hiddenWord.contains(userWord[i]) && hiddenWord[i]==userWord[i]){
                    print(green + "${userWord[i]}" + reset)
                }else if(hiddenWord.contains(userWord[i]) && hiddenWord[i]!==userWord[i]){
                    print(yellow + "${userWord[i]}" + reset)
                }else{
                    print(gray + "${userWord[i]}" + reset)
                }
            }
            println(" ")
            lives--
            if (lives == 0)game= false
        }else println("Has introducido una palabra invalida, prueba de nuevo")
    }while (game)
    if (win == true){
        println("Enorabuena! Ganaste")
    }else if(lives == 0){
        println("Oh no. Has perdido")
    }
}

